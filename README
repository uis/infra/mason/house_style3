               =======================================
               University House Style Mason components
               =======================================

                              ---------
                              Version 3
                              ---------

$Id$

This is a copy of the master documentation in the Computing Service's
Wiki at https://wiki.csx.cam.ac.uk/ucs/House_Style_3_README

These Mason components implement (part of) the University's web 'house
style'. Version 3 of these components impliment the www.cam/www.admin
style introduced in January 2008. The style has been adapted for
'Department' use - at present these components have only been adapted
for use within the Computing Service. Please contact
webmaster@cam.ac.uk before using these components anywhere
else. Please also note that there is MUCH more to producing documents
that conform to the house style than just using these components -
again, please contact webmaster@cam.ac.uk for advice.

Please see the file INSTALL for instructions on how to install the
components. 

There are two distinct ways to use these components:

  The 'high-level interface' uses a Mason autohandler to automatically
  'wrap' a series of page fragments with the house style. The
  fragments only contain page content and items of metadata about the
  coresponding web page. Facilities exist for including common
  elements (section title, breadcrumbs, left-hand navigation,
  tab-based navigation) in sets of related pages. This interface is
  documented in DOCS/high-level.txt

  The 'low-level interface' consists of a set of Mason components that
  create the various standard 'building blocks' of the house
  style. When embedded in apropriate page-level html, these components
  make it easy to create conforming pages. The autohandler that
  impliments the high-level interface itself uses these components;
  they are also suitable for use in other autohandlers or in other
  Mason-based web applications. This interface is documented in
  DOCS/low-level.txt

