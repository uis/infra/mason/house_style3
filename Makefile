MASON  = /var/www/mason-components/hs3
GLOBAL = /var/www/html/global

MFILES = $(MASON)/VERSION \
	 $(MASON)/htmlhead.mason \
	 $(MASON)/masthead.mason \
	 $(MASON)/breadcrumbs.mason \
	 $(MASON)/navitems.mason \
	 $(MASON)/banner.mason \
	 $(MASON)/sec-links.mason \
	 $(MASON)/sec-links-section.mason \
	 $(MASON)/printbox.mason \
	 $(MASON)/footer.mason \
	 $(MASON)/navtabs.mason \
	 $(MASON)/analytics-www-cam.mason \
	 $(MASON)/example-autohandler.mason

IFILES = $(GLOBAL)/images/bg-baseline.png \
	 $(GLOBAL)/images/bg-home-menu-top.gif \
	 $(GLOBAL)/images/bg-nav-primary-sub.jpg \
	 $(GLOBAL)/images/button-search.gif \
	 $(GLOBAL)/images/divider-site-tools-small.gif \
	 $(GLOBAL)/images/icon-alert-normal.gif \
	 $(GLOBAL)/images/icon-alert-warning.gif \
	 $(GLOBAL)/images/icon-arrow-home-menu.gif \
	 $(GLOBAL)/images/icon-arrow-nav-breadcrumb.gif \
	 $(GLOBAL)/images/icon-arrow-nav-primary.gif \
	 $(GLOBAL)/images/icon-arrow-secondary-menu.gif \
	 $(GLOBAL)/images/icon-audio.gif \
	 $(GLOBAL)/images/icon-community.gif \
	 $(GLOBAL)/images/icon-delicious.gif \
	 $(GLOBAL)/images/icon-digg.gif \
	 $(GLOBAL)/images/icon-down.gif \
	 $(GLOBAL)/images/icon-email.gif \
	 $(GLOBAL)/images/icon-external.gif \
	 $(GLOBAL)/images/icon-facebook.gif \
	 $(GLOBAL)/images/icon-follow.gif \
	 $(GLOBAL)/images/icon-help.gif \
	 $(GLOBAL)/images/icon-look.gif \
	 $(GLOBAL)/images/icon-media.gif \
	 $(GLOBAL)/images/icon-more.gif \
	 $(GLOBAL)/images/icon-photo.gif \
	 $(GLOBAL)/images/icon-quick-links.gif \
	 $(GLOBAL)/images/icon-reddit.gif \
	 $(GLOBAL)/images/icon-restricted.gif \
	 $(GLOBAL)/images/icon-rss.gif \
	 $(GLOBAL)/images/icon-section.gif \
	 $(GLOBAL)/images/icon-stumble.gif \
	 $(GLOBAL)/images/icon-top.gif \
	 $(GLOBAL)/images/icon-video.gif \
	 $(GLOBAL)/images/poweredby.gif \
	 $(GLOBAL)/images/test2.jpg \
	 $(GLOBAL)/images/test.jpg \
	 $(GLOBAL)/images/thumb-news.jpg \
	 $(GLOBAL)/images/2008020803.jpg \
	 $(GLOBAL)/images/banner-bird.jpg \
	 $(GLOBAL)/images/banner-mock-2.jpg \
	 $(GLOBAL)/images/banner-mock-3.jpg \
	 $(GLOBAL)/images/banner-mock.jpg \
	 $(GLOBAL)/images/banner-robinson.jpg \
	 $(GLOBAL)/images/banner-student.jpg \
	 $(GLOBAL)/images/bg-content-secondary-1px.gif \
	 $(GLOBAL)/images/bg-content-secondary-blue.gif \
	 $(GLOBAL)/images/bg-content-secondary-white.gif \
	 $(GLOBAL)/images/bg-more-info-blue.gif \
	 $(GLOBAL)/images/bg-more-info.gif \
	 $(GLOBAL)/images/bg-nav-primary-blue.jpg \
	 $(GLOBAL)/images/bg-spotlight-science.gif \
	 $(GLOBAL)/images/bg-tab-left-active.gif \
	 $(GLOBAL)/images/bg-tab-left-over.gif \
	 $(GLOBAL)/images/bg-tab-left.gif \
	 $(GLOBAL)/images/bg-tab-right-active.gif \
	 $(GLOBAL)/images/bg-tab-right-over.gif \
	 $(GLOBAL)/images/bg-tab-right.gif \
	 $(GLOBAL)/images/button2-bm.png \
	 $(GLOBAL)/images/distantfen.jpg \
	 $(GLOBAL)/images/fest-of-ideas.jpg \
	 $(GLOBAL)/images/foi.jpg \
	 $(GLOBAL)/images/icon-arrow-home-menu-orange.gif \
	 $(GLOBAL)/images/icon-arrow-nav-primary-orange.gif \
	 $(GLOBAL)/images/icon-arrow-nav-primary-white.gif \
	 $(GLOBAL)/images/icon-cal-large.gif \
	 $(GLOBAL)/images/icon-excel.gif \
	 $(GLOBAL)/images/icon-home.gif \
	 $(GLOBAL)/images/icon-media-large.gif \
	 $(GLOBAL)/images/icon-pdf.gif \
	 $(GLOBAL)/images/icon-rtf.gif \
	 $(GLOBAL)/images/icon-word.gif \
	 $(GLOBAL)/images/identifier4.gif \
	 $(GLOBAL)/images/news.gif \
	 $(GLOBAL)/images/next-white.gif \
	 $(GLOBAL)/images/next.gif \
	 $(GLOBAL)/images/next.png \
	 $(GLOBAL)/images/oldidentifier4.gif \
	 $(GLOBAL)/images/prev-white.gif \
	 $(GLOBAL)/images/previous.gif \
	 $(GLOBAL)/images/previous.png \
	 $(GLOBAL)/images/promo-bg-blue.gif \
	 $(GLOBAL)/images/staff.jpg \
	 $(GLOBAL)/images/uc.gif

JFILES = $(GLOBAL)/js/minmax.js \
	 $(GLOBAL)/js/banner.js \
	 $(GLOBAL)/js/jquery.js

SFILES = $(GLOBAL)/style/content.css \
	 $(GLOBAL)/style/forms.css \
	 $(GLOBAL)/style/ie6.css \
	 $(GLOBAL)/style/ie7.css \
	 $(GLOBAL)/style/layout.css \
	 $(GLOBAL)/style/print.css \
	 $(GLOBAL)/style/reset.css \
	 $(GLOBAL)/style/search-results.css \
	 $(GLOBAL)/style/typography.css

install: mason img js style

mason: $(MFILES) 

$(MFILES) : $(MASON)/% : %
	mkdir -p $(MASON)
	cp $< $@.new
	mv $@.new $@
	chmod ug+rw,o+r $@

img: $(IFILES) 

$(IFILES) : $(GLOBAL)/images/% : global/images/%
	mkdir -p $(GLOBAL)/images/
	cp $< $@.new
	mv $@.new $@
	chmod ug+rw,o+r $@

js: $(JFILES)

$(JFILES) : $(GLOBAL)/js/% : global/js/%
	mkdir -p $(GLOBAL)/js/
	cp $< $@.new
	mv $@.new $@
	chmod ug+rw,o+r $@

style: $(SFILES)

$(SFILES) : $(GLOBAL)/style/% : global/style/%
	mkdir -p $(GLOBAL)/style/
	cp $< $@.new
	mv $@.new $@
	chmod ug+rw,o+r $@

DISTDIR=hs3_v`cat VERSION`
BUILDDIR=$(DISTDIR)
TARFILE=$(DISTDIR).tgz

tar:
	mkdir -p $(BUILDDIR) && rm -rf $(BUILDDIR)/*
	mkdir $(BUILDDIR)/global/
	mkdir $(BUILDDIR)/global/images
	mkdir $(BUILDDIR)/global/style
	mkdir $(BUILDDIR)/global/js
	while read f; do cp $$f $(BUILDDIR)/$$f; done < MANIFEST
	tar zcf $(TARFILE) $(DISTDIR)
	rm -rf $(BUILDDIR)
